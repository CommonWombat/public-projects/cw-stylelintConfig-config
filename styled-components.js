"use strict";

module.exports = {
  processors: ['stylelint-processor-styled-components'],
  extends: [
    require.resolve('./index.js'),
    'stylelint-config-styled-components',
  ],

  plugins: [
    'stylelint-scss',
  ],
  rules: {
  },
};
