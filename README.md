stylelint-config-wombat
===================

## Installation
1. Installation

     yarn

    ```bash
    yarn add -D stylelint@^13.6.1 stylelint-config-wombat
    ```

    styled-components

    ```bash
    yarn add -D stylelint@^13.6.1 stylelint-config-wombat stylelint-processor-styled-components stylelint-config-styled-components
    ```
2. create or update `.stylelintrc`
    ```json
    {
      "extends": [
        "stylelint-config-wombat"
      ]
      ...
    }
    ```

    styled-components

    ```json
    {
      "extends": [
        "stylelint-config-wombat/styled-components"
      ]
      ...
    }
    ```
3. Add script to package.json 
    ```json
    "scripts": {
      ...
      "lint:styles": "stylelint src/**/*.{js,jsx,tsx}",
    }  
    ```

## Publishing
1. Be repo Maintainer.
2. Merging branch develop to master via Merge Request.
3. Switch locally to master branch
4. run `yarn run createNewVersion`
5. Select new version
6. after publication is completed make sure that everything is pushed to gitlab
7. merge master to develop(without creating Merge Request)
8. done
