"use strict";

module.exports = {
  plugins: [
    'stylelint-scss',
  ],
  rules: {
    "indentation": 2,
    "at-rule-no-unknown": null,
    "scss/at-rule-no-unknown": true,
    'block-no-empty': true,
    'color-no-invalid-hex': true,
    'comment-no-empty': true,
    'declaration-block-no-duplicate-properties': [
      true,
      {
        ignore: ['consecutive-duplicates-with-different-values']
      }
    ],
    'declaration-block-no-shorthand-property-overrides': true,
    'font-family-no-duplicate-names': true,
    'font-family-no-missing-generic-family-keyword': true,
    'function-calc-no-unspaced-operator': true,
    'function-linear-gradient-no-nonstandard-direction': true,
    'keyframe-declaration-no-important': true,
    'media-feature-name-disallowed-list': [
      "max-width",
      {
        "message": "Use min-width for a mobile-first approach (media-feature-name-blacklist)",
      },
    ],
    'media-feature-name-no-unknown': true,
    'no-descending-specificity': true,
    'no-duplicate-at-import-rules': true,
    'no-duplicate-selectors': true,
    'no-empty-source': true, 'no-extra-semicolons': true,
    'no-invalid-double-slash-comments': true,
    'property-no-unknown': true,
    'selector-pseudo-class-no-unknown': [true, { "ignorePseudoClasses": ["global"] }],
    'selector-pseudo-element-no-unknown': true,
    'selector-type-no-unknown': true,
    'string-no-newline': true,
    'unit-no-unknown': true,
    'number-leading-zero': 'always',
    'declaration-property-unit-allowed-list': {
      'font-size': ['em', 'rem'],
      '/^animation/': ['ms'],
      '/^transition/': ['ms'],
    },
    'max-nesting-depth': 3,
    'length-zero-no-unit': true,
    'declaration-colon-space-after': 'always-single-line',
  }
};
